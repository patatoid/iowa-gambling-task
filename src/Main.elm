module Main exposing (..)

import Browser
import Browser.Events exposing (onAnimationFrame)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html exposing (Html)
import Random exposing (..)
import Task exposing (..)
import Time exposing (Posix)



---- MODEL ----
-- On each trial, participants have to pick a card one deck. Each deck have different risk associated with.


type alias Card =
    { risk : RiskProfile
    , penalty : Penalty
    , reward : Reward
    , deckLabel : Label
    , time : Maybe Time.Posix
    }


type Status
    = Intro
    | Task
    | End


type Label
    = A
    | B
    | C
    | D


type RiskProfile
    = HighRisk
    | LowRisk


type Penalty
    = LowPenalty Int
    | HighPenalty Int
    | NoPenalty Int


type Reward
    = HighReward Int
    | LowReward Int


highPenalty =
    250


lowPenalty =
    50


noPenalty =
    0


highReward =
    100


lowReward =
    50



-- This is the amount of money the participant has at the beginning of the task.


loan =
    2000


maxTrial =
    10


type alias Model =
    { profit : Int
    , data : List Card
    , timeline : Int
    , modale : Bool
    , taskStatus : Status
    , modaleInfo :
        Maybe
            { win : Int
            , loose : Int
            }
    , trialCounter : Int
    }


init : ( Model, Cmd Msg )
init =
    ( { profit = loan
      , data = []
      , timeline = 0
      , modale = False
      , modaleInfo = Nothing
      , taskStatus = Task
      , trialCounter = 0
      }
    , Cmd.none
    )



---- UPDATE ----


type Msg
    = NoOp
    | CardPicked Card
    | GotPenalty Card
    | GotTime Card Time.Posix
    | OnTick Posix
    | GotNewStatus Status


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        CardPicked card ->
            ( model
            , Cmd.batch [ Random.generate GotPenalty (generatePenalty card) ]
            )

        GotPenalty card ->
            ( model
            , Task.perform (\time -> GotTime card time) Time.now
            )

        GotTime card time ->
            if model.trialCounter == maxTrial then
                update (GotNewStatus End) model

            else
                ( { model
                    | profit = model.profit - getPenaltyAmount card.penalty + getRewardAmount card.reward
                    , data = { card | time = Just time } :: model.data
                    , modale = True
                    , trialCounter = model.trialCounter + 1
                    , modaleInfo =
                        Just
                            { win = getRewardAmount card.reward
                            , loose = getPenaltyAmount card.penalty
                            }
                  }
                , Cmd.none
                )

        OnTick tick ->
            ( { model
                | timeline =
                    if model.timeline > 100 then
                        model.timeline - model.timeline

                    else
                        model.timeline + 1
                , modale =
                    if model.timeline == 100 then
                        not model.modale

                    else
                        model.modale
              }
            , Cmd.none
            )

        GotNewStatus status ->
            case status of
                Intro ->
                    ( { model
                        | taskStatus = Intro
                        , data = []
                        , trialCounter = 0
                        , modaleInfo = Nothing
                      }
                    , Cmd.none
                    )

                Task ->
                    ( { model | taskStatus = Task }, Cmd.none )

                End ->
                    ( { model | taskStatus = End }, Cmd.none )



---- VIEW ----


view : Model -> Html Msg
view model =
    let
        green =
            rgb255 116 185 131

        black =
            rgb255 0 0 0

        white =
            rgb255 255 255 255
    in
    case model.taskStatus of
        Task ->
            layout [] <|
                column
                    [ Element.width (px screen.width)
                    , Element.height (px screen.height)
                    , Border.width 2
                    , centerX
                    , centerY
                    ]
                    [ Element.el [ Font.size 18, alignRight, padding 20, Font.bold ] (text <| String.fromInt model.trialCounter ++ "/" ++ String.fromInt maxTrial)
                    , row
                        [ centerX
                        , centerY
                        , spacing 30
                        ]
                        [ viewDeck A HighRisk (HighReward highReward)
                        , viewDeck B HighRisk (HighReward highReward)
                        , viewDeck C LowRisk (LowReward lowReward)
                        , viewDeck D LowRisk (LowReward lowReward)
                        ]
                    , row
                        [ centerX
                        , centerY
                        , padding 30
                        ]
                        [ Element.el
                            [ Font.bold
                            , Font.size 36
                            , inFront <| viewFeedback model
                            ]
                            (Element.text <| "$" ++ String.fromInt model.profit)
                        ]
                    ]

        Intro ->
            layout [] <|
                column
                    [ Element.width (px screen.width)
                    , Element.height (px screen.height)
                    , Border.width 2
                    , centerX
                    , centerY
                    ]
                    [ Element.textColumn [ spacing 10 ]
                        [ Element.el [ padding 10, Font.bold ]
                            (text <| "Iowa gambling task")
                        , Element.paragraph [ padding 30, alignLeft ] [ text <| "You have to earn as the much as possible in choosing the right cards" ]
                        ]
                    , Input.button [ centerX, Border.width 1, padding 10, mouseOver [ Background.color black, Font.color white ] ]
                        { onPress = Just (GotNewStatus Task)
                        , label = Element.el [ Font.size 18 ] (Element.text "Click here to start the task")
                        }
                    ]

        End ->
            layout [] <|
                column
                    [ Element.width (px screen.width)
                    , Element.height (px screen.height)
                    , Border.width 2
                    , centerX
                    , centerY
                    ]
                    [ Element.textColumn [ spacing 10 ]
                        [ Element.el [ padding 10, Font.bold ]
                            (text <| "Iowa gambling task")
                        , Element.paragraph [ padding 30, alignLeft ] [ text <| "Thanks for your participation! You have been wise so far. This is good." ]
                        ]
                    , Input.button [ centerX, Border.width 1, padding 10, mouseOver [ Background.color black, Font.color white ] ]
                        { onPress = Just (GotNewStatus Intro)
                        , label = Element.el [ Font.size 18 ] (Element.text "Click here to start again ! ")
                        }
                    ]


viewFeedback : Model -> Element msg
viewFeedback model =
    let
        green =
            rgb255 116 185 131

        red =
            rgb255 200 7 7
    in
    if model.modale == True then
        case model.modaleInfo of
            Just info ->
                Element.row [ moveRight 110, Font.size 16 ]
                    [ Element.el [ Font.color red, Font.bold ] (text <| "- $" ++ String.fromInt info.loose ++ " ")
                    , Element.el [ Font.color green, Font.bold ] (text <| " +$" ++ String.fromInt info.win ++ " ")
                    ]

            Nothing ->
                Element.el [] none

    else
        Element.el [] none


viewDeck : Label -> RiskProfile -> Reward -> Element Msg
viewDeck label risk reward =
    let
        card =
            { risk = risk
            , penalty = NoPenalty 0
            , deckLabel = label
            , reward = reward
            , time = Nothing
            }

        grey =
            rgb255 61 61 85
    in
    Input.button
        [ Border.width 1
        , paddingEach { bottom = 30, left = 20, right = 20, top = 30 }
        , Border.rounded 3
        , mouseOver
            [ moveUp 5
            , Border.shadow { offset = ( 0, 4 ), size = 1, blur = 15, color = grey }
            ]
        ]
        { onPress = Just (CardPicked card)
        , label = Element.el [ Font.size 46 ] (Element.text (labelToString card.deckLabel))
        }



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = subscriptions
        }



---- SUBSCRIPTIONS ----


subscriptions model =
    Sub.batch
        [ if model.modale == True then
            onAnimationFrame OnTick

          else
            Sub.none
        ]



---- SCREEN ----


type alias Dimension =
    { width : Int, height : Int }


screen : Dimension
screen =
    { width = 500, height = 500 }



---- UTILS ----


getPenaltyAmount : Penalty -> Int
getPenaltyAmount penalty =
    case penalty of
        LowPenalty int ->
            int

        HighPenalty int ->
            int

        NoPenalty int ->
            int


getRewardAmount : Reward -> Int
getRewardAmount reward =
    case reward of
        HighReward int ->
            int

        LowReward int ->
            int


labelToString : Label -> String
labelToString label =
    case label of
        A ->
            "♠️"

        B ->
            "♥️"

        C ->
            "♣️"

        D ->
            "♦️"



-- This function associates a penalty to any pickable card.


generatePenalty : Card -> Random.Generator Card
generatePenalty card =
    case card.risk of
        HighRisk ->
            Random.int 0 100
                |> Random.andThen
                    (\int ->
                        if int < 50 then
                            Random.map
                                (\penalty ->
                                    { risk = card.risk
                                    , penalty = penalty
                                    , deckLabel = card.deckLabel
                                    , reward = card.reward
                                    , time = Nothing
                                    }
                                )
                                (Random.uniform (HighPenalty highPenalty) [])

                        else
                            Random.map
                                (\penalty ->
                                    { risk = card.risk
                                    , penalty = penalty
                                    , deckLabel = card.deckLabel
                                    , reward = card.reward
                                    , time = Nothing
                                    }
                                )
                                (Random.uniform (NoPenalty noPenalty) [])
                    )

        LowRisk ->
            Random.int 0 100
                |> Random.andThen
                    (\int ->
                        if int < 50 then
                            Random.map
                                (\penalty ->
                                    { risk = card.risk
                                    , penalty = penalty
                                    , deckLabel = card.deckLabel
                                    , reward = card.reward
                                    , time = Nothing
                                    }
                                )
                                (Random.uniform (LowPenalty lowPenalty) [])

                        else
                            Random.map
                                (\penalty ->
                                    { risk = card.risk
                                    , penalty = penalty
                                    , deckLabel = card.deckLabel
                                    , reward = card.reward
                                    , time = Nothing
                                    }
                                )
                                (Random.uniform (NoPenalty noPenalty) [])
                    )
