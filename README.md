

## Iowa gambling task

Decision-making study.

People are asked to choose one out of four card decks. People can earn or loose money when picking a card on a deck.

## Set-up

->   ```git clone  https://gitlab.com/YoelisA/implicit-association-task.git```

-> ``` npm install -g elm````

->  ``` npm install -g create-elm-app```

-> ```cd elm-gambling```

->  ```elm-app start```
